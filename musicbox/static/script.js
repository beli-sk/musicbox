function loadPlaylist() {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("playlist").innerHTML = this.responseText;
		}
	};
	xhttp.open("GET", "playlist", true);
	xhttp.send();
}

function loadVolume() {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("volume").innerHTML = this.responseText;
		}
	};
	xhttp.open("GET", "volume", true);
	xhttp.send();
}

function sendPlayNew() {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("play_field").value = '';
			loadPlaylist();
		}
	};
	xhttp.open("POST", document.forms['play'].action, true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send("value="+document.getElementById('play_field').value);
	return false;
}

function sendStop() {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			loadPlaylist();
		}
	};
	xhttp.open("POST", "stop", true);
	xhttp.send();
	return false;
}

function sendJump() {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("jump_field").value = '';
			loadPlaylist();
		}
	};
	xhttp.open("POST", document.forms['jump'].action, true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send("value="+document.getElementById('jump_field').value);
	return false;
}

function sendVolume() {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			loadVolume();
		}
	};
	xhttp.open("POST", document.forms['volume'].action, true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send("value="+document.getElementById('volume_field').value);
	return false;
}

function sendRemove(pos) {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			loadPlaylist();
		}
	};
	xhttp.open("POST", "remove", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send("value="+pos);
}

function reloadAll() {
	loadPlaylist();
	loadVolume();
}

setInterval(reloadAll, 5000);

