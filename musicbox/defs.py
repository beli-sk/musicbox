__version__ = '0.0.1'
app_name = 'musicbox'
app_description = 'Web interface for playing Youtube music with MPV'
app_name_desc = '{} - {}'.format(app_name, app_description)
