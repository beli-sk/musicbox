import json
import atexit
import queue
import threading
import subprocess

from redis import StrictRedis
from flask import Flask, g, request, redirect, url_for, render_template, make_response
from flask.views import MethodView

from .mpvremote import MPVRemote

mpv = None
title_thread = None
title_requests = queue.Queue()
redis = StrictRedis(charset="utf-8", decode_responses=True)

def title_loader():
    while True:
        url = title_requests.get()
        if url is None:
            break
        if redis.get(url) is not None:
            continue
        app.logger.info('getting title for %s', url)
        try:
            title = subprocess.check_output(['/usr/bin/youtube-dl', '--get-title', '--skip-download', url]).decode('utf-8').strip()
        except:
            app.logger.exception('get title failed for %s', url)
        else:
            redis.set(url, title)

def create_app():
    global mpv
    global title_thread
    app = Flask(__name__)

    mpv = MPVRemote('/tmp/mpvsocket')

    title_thread = threading.Thread(target=title_loader)
    title_thread.setDaemon(True)
    title_thread.start()

    def cleanup():
        global mpv
        app.logger.debug('cleanup')
        title_requests.put(None)
        if mpv is not None:
            mpv.close()
            mpv = None
        title_thread.join()

    atexit.register(cleanup)

    return app

app = create_app()

app.config.update(
        DEBUG=True,
        )

def get_title(url):
    title = redis.get(url)
    if title is None:
        return '[unknown]'
    else:
        return title

@app.route("/play", methods=['POST'])
def play():
    url = request.form['value']
    title_requests.put(url)
    mpv.command('loadfile', url, 'append-play')
    return redirect(url_for('index'))

@app.route("/remove", methods=['POST'])
def remove():
    pos = int(request.form['value'])
    mpv.command('playlist-remove', pos)
    return redirect(url_for('index'))

@app.route("/jump", methods=['POST'])
def jump():
    pos = int(request.form['value'])
    mpv.set_property('playlist-pos', pos)
    return redirect(url_for('index'))

@app.route("/stop", methods=['POST'])
def stop():
    mpv.command('stop')
    return redirect(url_for('index'))

@app.route("/volume", methods=['GET', 'POST'])
def volume():
    if request.method == 'GET':
        volume = int(mpv.get_property('volume'))
        resp = make_response(str(volume))
        resp.headers['Content-type'] = 'text/plain'
        return resp
    elif request.method == 'POST':
        volume = int(request.form['value'])
        mpv.set_property('volume', volume)
        return redirect(url_for('index'))

@app.route("/save", methods=['POST'])
def save():
    playlist = mpv.get_property('playlist')
    pipe = redis.pipeline()
    pipe.delete('playlist')
    for item in playlist:
        pipe.rpush('playlist', item['filename'])
    pipe.execute()
    return redirect(url_for('index'))

@app.route("/load", methods=['POST'])
def load():
    playlist = redis.lrange('playlist', 0, -1)
    mpv.command('playlist-clear')
    for item in playlist:
        mpv.command('loadfile', item, 'append')
        title_queue.put(item)
    mpv.set_property('playlist-pos', 0)
    return redirect(url_for('index'))

@app.route("/playlist")
def playlist():
    playlist = mpv.get_property('playlist')
    for item in playlist:
        if 'title' not in item:
            item['title'] = get_title(item['filename'])
    return render_template('playlist.html', playlist=playlist)

@app.route("/")
def index():
    volume = int(mpv.get_property('volume'))
    playlist = mpv.get_property('playlist')
    for item in playlist:
        if 'title' not in item:
            item['title'] = get_title(item['filename'])
    return render_template('index.html', volume=volume, playlist=playlist)
