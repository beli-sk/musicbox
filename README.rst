Music Box
=========

Web interface for playing Youtube music with MPV.

Locations
---------

The `project page`_ is hosted on Bitbucket.

If you've never contributed to a project on Bitbucket, there is
a `quick start guide`_.

If you find something wrong or know of a missing feature, please
`create an issue`_ on the project page. If you find that inconvenient or have
some security concerns, you could also drop me a line at <devel@beli.sk>.

.. _project page:         https://bitbucket.org/beli-sk/musicbox
.. _quick start guide:    https://www.atlassian.com/git/tutorials/making-a-pull-request
.. _create an issue:      https://bitbucket.org/beli-sk/musicbox/issues

License
-------

Copyright 2017 Michal Belica <devel@beli.sk>

::

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Aferro General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

A copy of the license text can be found in the ``LICENSE`` file in the
distribution.

